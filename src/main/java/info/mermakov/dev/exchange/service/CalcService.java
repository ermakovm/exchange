package info.mermakov.dev.exchange.service;

import info.mermakov.dev.exchange.exception.InvalidArgumentException;
import info.mermakov.dev.exchange.model.Client;
import info.mermakov.dev.exchange.model.Journal;
import info.mermakov.dev.exchange.model.Request;
import info.mermakov.dev.exchange.util.IDGenerator;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class CalcService {
    private final File inputBalance;
    private final File inputRequest;
    private final File outputBalance;
    private final File outputJournal;

    private Set<Client> clients;
    private List<Request> sellRequests;
    private List<Request> buyRequests;
    private List<Journal> journals;
    private Set<String> successRequests;

    public CalcService(File inputBalance, File inputRequest, File outputBalance, File outputJournal) {
        this.inputBalance = inputBalance;
        this.inputRequest = inputRequest;
        this.outputBalance = outputBalance;
        this.outputJournal = outputJournal;

        clients = new LinkedHashSet<>();
        journals = new LinkedList<>();
        sellRequests = new ArrayList<>();
        buyRequests = new ArrayList<>();
        successRequests = new HashSet<>();
    }

    private void readData() {
        ClientReader reader = new ClientReader(inputBalance);
        reader.readClients(clients);

        RequestReader requestReader = new RequestReader(inputRequest);
        requestReader.readOrder(sellRequests, buyRequests);
    }

    public void execute() {
        readData();
        sellRequests.sort(Request::compareTo);
        buyRequests.sort(Request::compareTo);

        for (Request sellRequest : sellRequests) {
            if (successRequests.contains(sellRequest.getId())) {
                continue;
            }
            for (Request buyRequest : buyRequests) {
                if (successRequests.contains(buyRequest.getId())) {
                    continue;
                }
                if (sellRequest.getAssetCode().equals(buyRequest.getAssetCode())) {
                    Long sellerId = sellRequest.getClientId();
                    Long buyerId = buyRequest.getClientId();
                    String code = sellRequest.getAssetCode();

                    if (sellRequest.getAssetCount() >= buyRequest.getAssetCount()) {
                        Long count = buyRequest.getAssetCount();
                        Client seller = null;
                        Client buyer = null;
                        for (Client client : clients) {
                            if (client.getId().equals(sellerId)
                                    && client.getAssetCode().equals(code)
                            ) {
                                seller = client;
                            }
                            if (client.getId().equals(buyerId)
                                    && client.getAssetCode().equals(code)) {
                                buyer = client;
                            }
                        }
                        if (seller != null && buyer != null) {
                            if (seller.getAssetCount() - count < 0) {
                                throw new InvalidArgumentException();
                            }
                            seller.setAssetCount(seller.getAssetCount() - count);
                            buyer.setAssetCount(buyer.getAssetCount() + count);
                        } else {
                            throw new InvalidArgumentException();
                        }

                        successRequests.add(buyRequest.getId());
                        successRequests.add(sellRequest.getId());

                        Journal journal = new Journal(IDGenerator.getId(), sellRequest.getId(), buyRequest.getId(), count);
                        journals.add(journal);
                    }
                }
            }
        }

        writeResult();
    }

    private void writeResult() {
        ClientWriter writer = new ClientWriter(outputBalance);
        writer.writeClients(clients);

        JournalWriter journalWriter = new JournalWriter(outputJournal);
        journalWriter.writeJournal(journals);
    }
}
