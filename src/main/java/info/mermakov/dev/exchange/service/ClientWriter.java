package info.mermakov.dev.exchange.service;

import info.mermakov.dev.exchange.model.Client;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Set;

public class ClientWriter {
    private final File file;

    public ClientWriter(File file) {
        this.file = file;
    }

    public void writeClients(Set<Client> clients) {
        try (FileOutputStream fileOutputStream = new FileOutputStream(file)) {
            try (BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(fileOutputStream))) {
                for (Client client : clients) {
                    writer.write(client.toString());
                    writer.newLine();
                }
            }
        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }
}
