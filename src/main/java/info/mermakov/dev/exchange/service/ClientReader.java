package info.mermakov.dev.exchange.service;

import info.mermakov.dev.exchange.model.Client;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Set;

public class ClientReader {
    private final File file;

    public ClientReader(File file) {
        this.file = file;
    }

    public void readClients(Set<Client> clients) {
        try (InputStreamReader streamReader = new InputStreamReader(
                new FileInputStream(file))) {
            CSVParser parser = CSVFormat.DEFAULT.parse(streamReader);
            for (CSVRecord record : parser) {
                long id = Long.parseLong(record.get(0));
                String code = record.get(1);
                long count = Long.parseLong(record.get(2));

                Client client = new Client(id, code, count);
                clients.add(client);
            }
        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }
}
