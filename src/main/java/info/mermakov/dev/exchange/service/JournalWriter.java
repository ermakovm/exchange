package info.mermakov.dev.exchange.service;

import info.mermakov.dev.exchange.model.Journal;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.List;

public class JournalWriter {
    private final File file;

    public JournalWriter(File file) {
        this.file = file;
    }

    public void writeJournal(List<Journal> journals) {
        try (FileOutputStream fileOutputStream = new FileOutputStream(file)) {
            try (BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(fileOutputStream))) {
                for (Journal journal : journals) {
                    writer.write(journal.toString());
                    writer.newLine();
                }
            }
        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }
}
