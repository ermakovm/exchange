package info.mermakov.dev.exchange.service;

import info.mermakov.dev.exchange.exception.InvalidTypeException;
import info.mermakov.dev.exchange.model.Request;
import info.mermakov.dev.exchange.model.TransactionType;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

public class RequestReader {
    private final File file;

    public RequestReader(File file) {
        this.file = file;
    }

    public void readOrder(List<Request> sellRequests, List<Request> buyRequests) {
        try (InputStreamReader streamReader = new InputStreamReader(
                new FileInputStream(file))) {
            CSVParser parser = CSVFormat.DEFAULT.parse(streamReader);

            for (CSVRecord record : parser) {
                String id = record.get(0);
                long clientId = Long.parseLong(record.get(1));
                String type = record.get(2);
                String code = record.get(3);
                long count = Long.parseLong(record.get(4));

                if (type.equals("BUY")) {
                    buyRequests.add(new Request(id, clientId, TransactionType.BUY, code, count));
                } else if (type.equals("SELL")) {
                    sellRequests.add(new Request(id, clientId, TransactionType.SELL, code, count));
                } else {
                    throw new InvalidTypeException();
                }
            }
        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }
}
