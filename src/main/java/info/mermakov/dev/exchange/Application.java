package info.mermakov.dev.exchange;

import info.mermakov.dev.exchange.service.CalcService;

import java.io.File;

public class Application {
    private static final String INPUT_BALANCE_FILENAME = "input/balance.csv";
    private static final String INPUT_REQUEST_FILENAME = "input/request.csv";
    private static final String OUTPUT_BALANCE_FILENAME = "output/balance.csv";
    private static final String OUTPUT_JOURNAL_FILENAME = "output/journal.csv";

    public static void main(String[] args) {
        File inputBalance;
        File inputRequest;
        File outputBalance;
        File outputJournal;

        if (args.length == 4) {
            inputBalance = new File(args[0]);
            inputRequest = new File(args[1]);
            outputBalance = new File(args[2]);
            outputJournal = new File(args[3]);
        } else {
            inputBalance = new File(INPUT_BALANCE_FILENAME);
            inputRequest = new File(INPUT_REQUEST_FILENAME);
            outputBalance = new File(OUTPUT_BALANCE_FILENAME);
            outputJournal = new File(OUTPUT_JOURNAL_FILENAME);
        }

        CalcService service = new CalcService(inputBalance, inputRequest, outputBalance, outputJournal);
        service.execute();
    }
}
