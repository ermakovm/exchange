package info.mermakov.dev.exchange.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.Objects;

@Getter
@Setter
@AllArgsConstructor
public class Client {
    private Long id;
    private String assetCode;
    private Long assetCount;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Client client = (Client) o;
        return id.equals(client.id) &&
                assetCode.equals(client.assetCode);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, assetCode);
    }

    @Override
    public String toString() {
        return id + "," + assetCode + "," + assetCount;
    }
}
