package info.mermakov.dev.exchange.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class Journal {
    @NonNull
    private String id;

    @NonNull
    private String sellRequestId;

    @NonNull
    private String buyRequestId;

    @NonNull
    private Long assetCount;

    @Override
    public String toString() {
        return id + "," + sellRequestId + "," + buyRequestId + "," + assetCount;
    }
}
