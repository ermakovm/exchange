package info.mermakov.dev.exchange.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class Request implements Comparable<Request> {
    private String id;
    private Long clientId;
    private Enum type;
    private String assetCode;
    private Long assetCount;

    @Override
    public int compareTo(Request o) {
        return -1 * assetCount.compareTo(o.getAssetCount());
    }
}
