package info.mermakov.dev.exchange.model;

public enum TransactionType {
    BUY,
    SELL
}
