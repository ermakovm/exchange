package info.mermakov.dev.exchange.util;

import static java.util.UUID.randomUUID;

public class IDGenerator {
    public static String getId() {
        return randomUUID().toString()
                + "-"
                + System.currentTimeMillis();
    }
}
